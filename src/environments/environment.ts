// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyDEc0h0Z3hhJBOw1V-k2-3IoQw-Vm58iJM",
    authDomain: "oshop-33723.firebaseapp.com",
    databaseURL: "https://oshop-33723.firebaseio.com",
    projectId: "oshop-33723",
    storageBucket: "",
    messagingSenderId: "932972604682"
  }
};
